# git

distraction free hyper productive mouse free git workflows

```sh
git clone https://gitlab.com/shane0/git-workflows
```

* other [distraction free workflows](https://shanenull.com/cheatsheets/workflow/)
	* [todo cli cookiecutter](https://gitlab.com/shane0/cookiecutter-todocli)
	* [distraction free keyboards](https://shanenull.com/keyboards/)
	* manage all your cookiecutter templates with [click-cookiecutter](https://gitlab.com/shane0/cookiecutter-click-public)

* [x] screencast creating mouseless workflow creating local and uploading remote repos without leaving the terminal

[![asciicast](https://asciinema.org/a/jjw0j3nE1n74EmNTcYtTor2p1.svg)](https://asciinema.org/a/jjw0j3nE1n74EmNTcYtTor2p1)

